#!/bin/env python

def fab(index):
	if index == 0: return 0
	elif index == 1: return 1
	else: return fab(index - 1) + fab(index - 2)

for i in range(10):
	print fab(i)
